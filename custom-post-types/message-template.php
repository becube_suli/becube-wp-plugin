<?php

/***********************************************************************
 * Create custom post type                                             *
 ***********************************************************************/

/**
 * Creates the custom post type
 */
function becube_create_message_template_posttype() {
    register_post_type( 'message_templates',
        array(
            'labels' => array(
                'name' => __( 'Üzenetsablonok' ),
                'singular_name' => __( 'Üzenetsablon' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'uzenetsablonok'),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-email',
 
        )
    );
}

// Hooks the creation of custom post type to the init action
add_action( 'init', 'becube_create_message_template_posttype' );

/***********************************************************************
 * Add metadata and meta boxes                                         *
 ***********************************************************************/

/**
 * Adds metabox(es) to the edit screen
 */
function becube_add_message_template_metaboxes() {
}

/**
 * Renders the added metabox(es)
 */
function becube_message_template_GEGEGE_metabox_display( $post ) {

}


/***********************************************************************
 * Save metadata                                                       *
 ***********************************************************************/
/**
 * Writes the contents of the form to the database
 */
function becube_save_message_template_post_data($post_id)
{

}

// Hook the saving function to the save_post action
add_action('save_post', 'becube_save_message_template_post_data');

?>