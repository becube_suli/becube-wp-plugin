<?php

/***********************************************************************
 * Create custom post type                                             *
 ***********************************************************************/

/**
 * Creates the custom post type
 */
function becube_create_payment_posttype() {
    register_post_type( 'payments',
        array(
            'labels' => array(
                'name' => __( 'Befizetések' ),
                'singular_name' => __( 'Befizetés' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'befizetesek'),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-yes',
 
        )
    );
}

// Hooks the creation of custom post type to the init action
add_action( 'init', 'becube_create_payment_posttype' );

/***********************************************************************
 * Add metadata and meta boxes                                         *
 ***********************************************************************/

/**
 * Adds metabox(es) to the edit screen
 */
function becube_add_payment_metaboxes() {

}

/**
 * Renders the added metabox(es)
 */
function becube_payment_GEGEGE_metabox_display( $post ) {

}


/***********************************************************************
 * Save metadata                                                       *
 ***********************************************************************/
/**
 * Writes the contents of the form to the database
 */
function becube_save_payment_post_data($post_id)
{

}

// Hook the saving function to the save_post action
add_action('save_post', 'becube_save_payment_post_data');

?>