<?php

// TODO create array for the dropdown values (e.g. status)

/***********************************************************************
 * Create custom post type                                             *
 ***********************************************************************/

/**
 * Represents information about an application states
 */
class ApplicationState {

    public const INFO_SENT = "INFO_SENT";
    public const WAITING_LIST = "WAITING_LIST";
    public const OBSERVER = "OBSERVER";
    public const CANCELLED = "CANCELLED";
    public const IN_PROGRESS = "IN_PROGRESS";
    public const DISAPPEARED = "DISAPPEARED";
    public const DID_NOT_PAY = "DID_NOT_PAY";
    public const FINISHED = "FINISHED";

    public const POSSIBLE_STATES = array(
        self::INFO_SENT => "INFO levél kiment",
        self::WAITING_LIST => "Várólistán van",
        self::OBSERVER => "Megfigyelő",
        self::CANCELLED => "Lemondta",
        self::IN_PROGRESS => "Kurzus folyamatban",
        self::DISAPPEARED => "Nem jelzett vissza",
        self::DID_NOT_PAY => "Nem fizetett",
        self::FINISHED => "Elvégezte"
    );

    public const DEFAULT_STATE = "INFO_SENT";
 
    public static function the_dropdown_field($post) {
        
    	ob_start(); ?>
    
        <label for="becube_application_status_inputfield_label"><b>Státusz:</b> </label>

        <select name='becube_application_status_inputfield' id='becube_application_status_inputfield' value="<?php echo get_post_meta($post->ID, 'becube_application_status', true); ?>">            

            <?php foreach (self::POSSIBLE_STATES as $state => $string_state_representation) { ?>
                <option value="<?php echo $state ?>"<?php selected(get_post_meta($post->ID, 'becube_application_status', true), $state); ?>><?php echo $string_state_representation ?></option>
            <?php } ?>
        
        </select>

        <?php
        echo ob_get_clean();
    }

};

/**
 * Creates the custom post type
 */
function becube_create_application_posttype() {
    register_post_type( 'applications',
        array(
            'labels' => array(
                'name' => __( 'Jelentkezések' ),
                'singular_name' => __( 'Jelentkezés' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'jelentkezesek'),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-universal-access',
            'register_meta_box_cb' => 'becube_add_application_metaboxes',
            'supports' => array('title','thumbnail','excerpt','comments', 'revisions')
        )
    );
}

// Hooks the creation of custom post type to the init action
add_action( 'init', 'becube_create_application_posttype' );

/***********************************************************************
 * Add metadata and meta boxes                                         *
 ***********************************************************************/

/**
 * Adds metabox(es) to the edit screen
 */
function becube_add_application_metaboxes() {
	add_meta_box(
		'becube_application_information_metabox',
		'Részletek',
		'becube_application_information_metabox_display',
		'applications',
		'normal',
		'high'
    );
}

/**
 * Renders the added metabox where detailed but not editable information is displayed
 */
function becube_application_information_metabox_display( $post ) {
    ?>

    <div>

    <!-- Course code -->
    <p>
        <b>Tanfolyam betűjele:</b>
        
        <?php $value = get_post_meta($post->ID, 'becube_application_course_code', true); ?>
        <?php query_posts(array('post_type' => 'courses')); ?>

        <select name='my_meta_box_post_type' id='my_meta_box_post_type' value="<?php echo $value ?>">            

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <option value="<?php echo esc_attr(the_title()); ?>"<?php selected($value, get_the_title()); ?>><?php echo esc_html(the_title()); ?></option>

        <?php endwhile; endif; wp_reset_query(); ?>

        </select>
    </p>
    
    <!-- Applicant -->
    <p>
        <b>Jelentkező:</b>
        
        <?php 
        
        $applicant_user_id = get_post_meta($post->ID, 'becube_application_applicant', true);
        
        $args = array(
            'role'    => 'um_diak',
        );
        $users = get_users( $args ); ?>
        
        <select name='becube_application_applicant_inputfield' id='becube_application_applicant_inputfield' value="<?php echo $applicant_user_id ?>">

            <?php foreach ( $users as $user ) { ?>
                <option value="<?php echo esc_attr($user->id); ?>"<?php selected($applicant_user_id, $user->id); ?>><?php echo esc_html($user->display_name); ?></option>
            <?php } ?>
            
        </select>
    </p>

    <!-- Status -->
    <p>
        <?php ApplicationState::the_dropdown_field($post); ?>
    </p>

    <p><b>Jelentkező neve:</b> Horváth Pista</p>
    <p><b>Tanar neve:</b> Horváth Pista</p>
    <p><b>Tanfolyam típusa:</b> Kezdő programozó tanfolyam</p>
    <p><b>Helyszín:</b> Pannon Kincstár</p>
    <p><b>Órák időpontja:</b> 18:00 - 19:30</p>
    <p><b>Dátumok:</b></p>
    <ul>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
        <li>2020. märcius 15</li>
    </ul>

    </div>

    <?php
}

/***********************************************************************
 * Save metadata                                                       *
 ***********************************************************************/
/**
 * Writes the contents of the form to the database
 */
function becube_save_application_post_data($post_id)
{
    if (array_key_exists('my_meta_box_post_type', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_application_course_code',
            $_POST['my_meta_box_post_type']
        );
    }

    if (array_key_exists('becube_application_applicant_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_application_applicant',
            $_POST['becube_application_applicant_inputfield']
        );
    }

    if (array_key_exists('becube_application_status_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_application_status',
            $_POST['becube_application_status_inputfield']
        );
    }
}

// Hook the saving function to the save_post action
add_action('save_post', 'becube_save_application_post_data');

/**
 * Creates a new application type custom post with the given data.
 * 
 * @param string $title will be the title of the post
 * @param string $course_code must be a title of an existing "courses" type post
 *      The created application will belong to this course.
 * @param string $user_id must be an id of a registered, "student"
 *      type user. This user applied to the above mentioned
 *      course.
 * @param string $status (optional) The application will be created with this status.
 *      Defaults to "INFO_SENT". Must be one of the valid statuses.
 */
function create_application($title, $course_code, $user_id, $status = ApplicationState::INFO_SENT) {

    // TODO check if course exists

    // TODO check if user exists

    // TODO check if $status is one of the valid statuses

    $post_id = wp_insert_post(array (
        'post_title' => $title,
        'post_type' => 'applications',
        'post_status' => 'publish',
    
        'meta_input' => array(
            'becube_application_course_code' => $course_code,
            'becube_application_applicant' => $user_id,
            'becube_application_status' => $status
        )
    ));
}

?>