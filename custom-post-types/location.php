<?php

/***********************************************************************
 * Create custom post type                                             *
 ***********************************************************************/

/**
 * Creates the custom post type
 */
function becube_create_location_posttype() {
    register_post_type( 'locations',
        array(
            'labels' => array(
                'name' => __( 'Helyszínek' ),
                'singular_name' => __( 'Helyszín' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'helyszinek'),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-location',
            'register_meta_box_cb' => 'becube_add_location_metaboxes',
            'supports' => array('title','thumbnail','excerpt','comments', 'revisions')
        )
    );
}

// Hooks the creation of custom post type to the init action
add_action( 'init', 'becube_create_location_posttype' );

/***********************************************************************
 * Add metadata and meta boxes                                         *
 ***********************************************************************/

/*
TODO
INfo
Tanfolyamok listaja statusszal es datummal

Adatok
Egyedi azonosito
Helyszinleiras
*/

/**
 * Adds metabox(es) to the edit screen
 */
function becube_add_location_metaboxes() {
	add_meta_box(
		'becube_location_basic_metabox',
		'Alapadatok',
		'becube_location_basic_metabox_display',
		'locations',
		'normal',
		'high'
    );
}

/**
 * Renders the added metabox(es)
 */
function becube_location_basic_metabox_display( $post ) {
?>

    <!-- Status -->
    <p>
        <label for="becube_location_status_inputfield_label"><b>Státusz:</b> </label>

        <select name='becube_location_status_inputfield' id='becube_location_status_inputfield' value="<?php echo get_post_meta($post->ID, 'becube_location_status', true); ?>">            

            <option value="PLANNED"<?php selected(get_post_meta($post->ID, 'becube_location_status', true), "PLANNED"); ?>>Majd egyszer/talán</option>
            <option value="UNDER_NEGOTIATION"<?php selected(get_post_meta($post->ID, 'becube_location_status', true), "UNDER_NEGOTIATION"); ?>>Tárgyalás alatt</option>
            <option value="IN_PROGRESS"<?php selected(get_post_meta($post->ID, 'becube_location_status', true), "IN_PROGRESS"); ?>>Használatban</option>
            <option value="FINISHED"<?php selected(get_post_meta($post->ID, 'becube_location_status', true), "FINISHED"); ?>>Megszakítva</option>

        </select>

    </p>

    <!-- Address -->
    <p>
        <label for="becube_location_address_inputfield_label"><b>Cím:</b> </label>
        <input name="becube_location_address_inputfield" type="text" value="<?php echo get_post_meta($post->ID, 'becube_location_address', true); ?>">
    </p>

    <!-- Short Description -->
    <p>
    <label for="becube_location_shortdescription_inputfield_label"><b>Rövid helyszínleírás (honlapra):</b> </label><br>
    <textarea name="becube_location_shortdescription_inputfield" rows="4" cols="50">
<?php echo get_post_meta($post->ID, 'becube_location_shortdescription', true); ?>
    </textarea>
    </p>

    <!-- Description -->
    <p>
        <label for="becube_location_description_inputfield_label"><b>Részletes helyszínleírás (üzenetbe):</b> </label><br>
        <textarea name="becube_location_description_inputfield" rows="10" cols="50">
<?php echo get_post_meta($post->ID, 'becube_location_description', true); ?>
        </textarea>
    </p>

    <!-- Map URL -->
    <p>
        <label for="becube_location_mapurl_inputfield_label"><b>Térkép link:</b> </label>
        <input name="becube_location_mapurl_inputfield" type="url" value="<?php echo get_post_meta($post->ID, 'becube_location_mapurl', true); ?>">
    </p>

<?php
}


/***********************************************************************
 * Save metadata                                                       *
 ***********************************************************************/
/**
 * Writes the contents of the form to the database
 */
function becube_save_location_post_data($post_id)
{
    if (array_key_exists('becube_location_status_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_location_status',
            $_POST['becube_location_status_inputfield']
        );
    }

    if (array_key_exists('becube_location_address_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_location_address',
            $_POST['becube_location_address_inputfield']
        );
    }

    if (array_key_exists('becube_location_shortdescription_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_location_shortdescription',
            $_POST['becube_location_shortdescription_inputfield']
        );
    }

    if (array_key_exists('becube_location_description_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_location_description',
            $_POST['becube_location_description_inputfield']
        );
    }

    if (array_key_exists('becube_location_mapurl_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_location_mapurl',
            $_POST['becube_location_mapurl_inputfield']
        );
    }
}

// Hook the saving function to the save_post action
add_action('save_post', 'becube_save_location_post_data');

?>