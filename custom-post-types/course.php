<?php


/***********************************************************************
 * Helper functions                                                    *
 ***********************************************************************/

/**
 * Returns a human readable string representation of the becube_course_type meta field.
 * 
 * @param string $course_type The value of the becube_course_type field.
 * 
 * @return string the string representation (now in Hungarian) of the becube_course_type meta field.
 */
function get_description_for_course_type( $course_type ) {
    switch ( $course_type ) {
        case "BEGINNER_PROGRAMMING": return "Kezdő programozó tanfolyam";
        case "ADVANCED_PROGRAMMING": return "Haladó programozó tanfolyam";
        case "COMPANY_BEGINNER_PROGRAMMING": return "Céges kezdő programozó tanfolyam";
        case "COMPANY_ADVANCED_PROGRAMMING": return "Céges haladó programozó tanfolyam";
        case "BEGINNER_FRONTEND": return "Kezdő webfejlesztés tanfolyam";
        case "ONLINE_BEGINNER_FRONTEND": return "Online kezdő webfejlesztés tanfolyam";
        default: return "Error when parsing course type: [$course_type]";
    }
}

/**
 * Echoes a human readable string representation of the becube_course_type meta field.
 * 
 * @param string $course_type The value of the becube_course_type field.
 * 
 * @return NULL
 */
function the_description_for_course_type( $course_type ) {
    echo get_description_for_course_type( $course_type );
}

/**
 * Returns a human readable string representation of the becube_course_providecomputer meta field.
 * 
 * @param string $course_type The value of the becube_course_providecomputer field.
 * 
 * @return string the string representation (now in Hungarian) of the becube_course_providecomputer meta field.
 */
function get_description_for_providecomputer( $becube_course_providecomputer ) {
    switch ( $becube_course_providecomputer ) {
        case "COMPUTER_NOT_PROVIDED": return "Hozd-a-saját-géped";
        case "COMPUTER_PROVIDED": return "Igényelhetsz számítógépet";
        default: return "Error when parsing becube_course_providecomputer type: [$becube_course_providecomputer]";
    }
}

/**
 * Echoes a human readable string representation of the becube_course_providecomputer meta field.
 * 
 * @param string $course_type The value of the becube_course_providecomputer field.
 * 
 * @return NULL
 */
function the_description_for_providecomputer( $becube_course_providecomputer ) {
    echo get_description_for_providecomputer( $becube_course_providecomputer );
}

/**
 * Returns a Hungarian day representation of the given date.
 * 
 * It returns the string as "on every ...", e.g. if the given date is
 * on a Monday then the return value is "on every Monday" to be suitable
 * as a part of the course description. In practice you can give it the
 * first day of the course.
 * 
 * @param date $date any date
 * 
 * @return string Hungarian string representation of on which days the course is.
 */
function get_the_days( $date ) {
    $timestamp = strtotime($date);

    if ( !$timestamp ) {
        return "Error when parsing date: [$date]";
    } else {
        $weekday = date('N', $timestamp);
        switch ($weekday) {
            case 1: return "Hétfőnként";
            case 2: return "Keddenként";
            case 3: return "Szerdánként";
            case 4: return "Csütörtökönként";
            case 5: return "Péntekenként";
            case 6: return "Szombatonként";
            case 7: return "Vasárnaponként";
            default: return "Error when parsing weekday: [$weekday]";
        }
    }
}


/**
 * Echoes a Hungarian day representation of the given date.
 * 
 * It echoes the string as "on every ...", e.g. if the given date is
 * on a Monday then the return value is "on every Monday" to be suitable
 * as a part of the course description. In practice you can give it the
 * first day of the course.
 * 
 * @param date $date any date
 * 
 * @return NULL
 */
function the_days( $date ) {
    echo get_the_days( $date );
}

/**
 * Returns the application type posts that belong to the given course.
 */
function get_applications_to_this_course( $post ) {
    return get_posts(array(
        'post_type' => 'applications',
        'meta_key'   => 'becube_application_course_code',
        'meta_value' => get_the_title( $post )
    ));
}

/**
 * Returns the number of application type posts that belong to the given course.
 */
function get_current_headcount( $post ) {
    return count(get_applications_to_this_course( $post ));
}

/**
 * Returns TRUE if the number of current applicants is less than the maximum headcount
 * and FALSE otherwise.
 */
function is_there_free_place( $post ) {
    $max_headcount = get_field('becube_course_maxheadcount', $post);
    $current_headcount = get_current_headcount( $post );
    return $current_headcount < $max_headcount;
}

/**
 * Returns a Hungarian string descrption whether there are free place on the course
 * or there is a waiting list.
 */
function get_waiting_list_description( $post ) {
    if (is_there_free_place($post)) {
        return "Van szabad hely";
    } else {
        return "Várólista";
    }
}

/**
 * Echoes a Hungarian string descrption whether there are free place on the course
 * or there is a waiting list.
 */
function the_waiting_list_description( $post ) {
    echo get_waiting_list_description( $post );
}

/***********************************************************************
 * Create custom post type                                             *
 ***********************************************************************/

/**
 * Creates the custom post type
 */
function becube_create_course_posttype() {
    register_post_type( 'courses',
        array(
            'labels' => array(
                'name' => __( 'Tanfolyamok' ),
                'singular_name' => __( 'Tanfolyam' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'tanfolyamok'),
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-welcome-learn-more',
            'register_meta_box_cb' => 'becube_add_course_metaboxes',
            'supports' => array('title','thumbnail','excerpt','comments', 'revisions')
 
        )
    );
}

// Hooks the creation of custom post type to the init action
add_action( 'init', 'becube_create_course_posttype' );

/***********************************************************************
 * Add metadata and meta boxes                                         *
 ***********************************************************************/

/**
 * Adds metabox(es) to the edit screen
 */
function becube_add_course_metaboxes() {
	add_meta_box(
		'becube_course_information_metabox',
		'Jelentkezések',
		'becube_course_information_metabox_display',
		'courses',
		'normal',
		'high'
    );
	add_meta_box(
		'becube_course_data_metabox',
		'Adatok',
		'becube_course_data_metabox_display',
		'courses',
		'normal',
		'high'
    );
	add_meta_box(
		'becube_course_finantials_metabox',
		'Pénzügyek',
		'becube_course_finantials_metabox_display',
		'courses',
		'normal',
		'high'
    );
}

/**
 * Renders the added metabox(es)
 */
function becube_course_information_metabox_display( $post ) {
?>

<p><b>Aktuális létszám</b>: 12<?php /* get_headcount */ ?> / <?php echo get_post_meta($post->ID, 'becube_course_maxheadcount', true); ?> </p>

<p><b>Jelentkezettek</b>:
<ol>
<li>Jóska Pista</li>
<li>Béla Margit</li>
<li>Háklis János</li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ol>
<br>
Várólista:
<ol>
<li>Vévépapír Anita</li>
<li>Hunor Sztálin</li>
</ol>
Megfigyelő:
<ul>
<li>Ayrton Senna</li>
</ul>
</p>

<?php
}

function becube_course_finantials_metabox_display( $post ) {
    /*
TODO maybe WP_List_table https://www.smashingmagazine.com/2011/11/native-admin-tables-wordpress/
Penzugyek
efizetesek
Teremarak
Tanarkoltseg
Profit
*/
?>
<p>
<b>Befizetések</b>:
<table>
    <tr>
        <td>Nev</td>
        <td>Osszeg</td>
        <td>Statusz</td>
        <td>Hatarido</td>
    </tr>
    <tr>
        <td>Nev</td>
        <td>Osszeg</td>
        <td>Statusz</td>
        <td>Hatarido</td>
    </tr>
    <tr>
        <td>Nev</td>
        <td>Osszeg</td>
        <td>Statusz</td>
        <td>Hatarido</td>
    </tr>
</table>
</p>

<p  >
</p>
<?php
}

/**
 * Renders the added metabox(es)
 */
function becube_course_data_metabox_display( $post ) {
?>
    <!-- Course type -->
    <p>
        <label for="becube_course_type_inputfield_label"><b>Tanfolyam típusa:</b> </label>

        <select name='becube_course_type_inputfield' id='becube_course_type_inputfield' value="<?php echo get_post_meta($post->ID, 'becube_course_type', true); ?>">            

            <option value="BEGINNER_PROGRAMMING"<?php selected(get_post_meta($post->ID, 'becube_course_type', true), "BEGINNER_PROGRAMMING"); ?>>Kezdő programozó tanfolyam</option>
            <option value="ADVANCED_PROGRAMMING"<?php selected(get_post_meta($post->ID, 'becube_course_type', true), "ADVANCED_PROGRAMMING"); ?>>Haladó programozó tanfolyam</option>
            <option value="COMPANY_BEGINNER_PROGRAMMING"<?php selected(get_post_meta($post->ID, 'becube_course_type', true), "COMPANY_BEGINNER_PROGRAMMING"); ?>>Céges kezdő programozó tanfolyam</option>
            <option value="COMPANY_ADVANCED_PROGRAMMING"<?php selected(get_post_meta($post->ID, 'becube_course_type', true), "COMPANY_ADVANCED_PROGRAMMING"); ?>>Céges haladó programozó tanfolyam</option>
            <option value="BEGINNER_FRONTEND"<?php selected(get_post_meta($post->ID, 'becube_course_type', true), "BEGINNER_FRONTEND"); ?>>Kezdő webfejlesztés tanfolyam</option>
            <option value="ONLINE_BEGINNER_FRONTEND"<?php selected(get_post_meta($post->ID, 'becube_course_type', true), "ONLINE_BEGINNER_FRONTEND"); ?>>Online kezdő webfejlesztés tanfolyam</option>

        </select>

    </p>

    <!-- Status -->
    <p>
        <label for="becube_course_status_inputfield_label"><b>Státusz:</b> </label>

        <select name='becube_course_status_inputfield' id='becube_course_status_inputfield' value="<?php echo get_post_meta($post->ID, 'becube_course_status', true); ?>">            

            <option value="UNDER_ORGANIZATION"<?php selected(get_post_meta($post->ID, 'becube_course_status', true), "UNDER_ORGANIZATION"); ?>>Szervezés alatt</option>
            <option value="APPLICATION_OPEN"<?php selected(get_post_meta($post->ID, 'becube_course_status', true), "APPLICATION_OPEN"); ?>>Jelentkezés nyitva</option>
            <option value="IN_PROGRESS"<?php selected(get_post_meta($post->ID, 'becube_course_status', true), "IN_PROGRESS"); ?>>Folyamatban</option>
            <option value="FINISHED"<?php selected(get_post_meta($post->ID, 'becube_course_status', true), "FINISHED"); ?>>Befejezett</option>
            <option value="CANCELLED_DUE_TO_NOT_ENOUGH_APPLICANTS"<?php selected(get_post_meta($post->ID, 'becube_course_status', true), "CANCELLED_DUE_TO_NOT_ENOUGH_APPLICANTS"); ?>>Lefújva létszámhiány miatt</option>
            <option value="CANCELLED_DUE_TO_OTHER_REASON"<?php selected(get_post_meta($post->ID, 'becube_course_status', true), "CANCELLED_DUE_TO_OTHER_REASON"); ?>>Lefújva más okból</option>

        </select>

    </p>

    <!-- Location -->
    <p>
        <b>Helyszín:</b>
        
        <?php $value = get_post_meta($post->ID, 'becube_course_location', true); ?>
        <?php query_posts(array('post_type' => 'locations')); ?>

        <select name='becube_course_location_inputfield' id='becube_course_location_inputfield' value="<?php echo $value ?>">            

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <option value="<?php echo esc_attr(the_title()); ?>"<?php selected($value, get_the_title()); ?>><?php echo esc_html(the_title()); ?></option>

        <?php endwhile; endif; wp_reset_query(); ?>

        </select>
    </p>

    <!-- Max headcount -->
    <p>
        <label for="becube_course_maxheadcount_inputfield_label"><b>Maximális létszám:</b></label>
        <input name="becube_course_maxheadcount_inputfield" type="number" value="<?php echo get_post_meta($post->ID, 'becube_course_maxheadcount', true); ?>">
    </p>

    <!-- Teacher -->
    <p>
        <b>Tanár:</b>
        
        <?php $value = get_post_meta($post->ID, 'becube_course_teacher', true); ?>
        <?php 
        $args = array(
            'role'    => 'um_tanar',
        );
        $users = get_users( $args ); ?>
        
        <select name='becube_course_teacher_inputfield' id='becube_course_teacher_inputfield' value="<?php echo $value ?>">

            <?php foreach ( $users as $user ) { ?>
                <option value="<?php echo esc_attr($user->display_name); ?>"<?php selected($value, $user->display_name); ?>><?php echo esc_html($user->display_name); ?></option>
            <?php } ?>
            
        </select>
    </p>

    <!-- Lecture start -->
    <p>
        <label for="becube_course_starttime_inputfield_label"><b>Órakezdés:</b></label>
        <input name="becube_course_starttime_inputfield" type="time" value="<?php echo get_post_meta($post->ID, 'becube_course_starttime', true); ?>">
    </p>

    <!-- Dates -->
    <p>
        <label for="becube_course_date_1"><b>Első óra:</b></label>
        <input name="becube_course_date_1_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_1', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_2"><b>Második óra:</b></label>
        <input name="becube_course_date_2_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_2', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_3"><b>Harmadik óra:</b></label>
        <input name="becube_course_date_3_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_3', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_4"><b>Negyedik óra:</b></label>
        <input name="becube_course_date_4_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_4', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_5"><b>Ötödik óra:</b></label>
        <input name="becube_course_date_5_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_5', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_6"><b>Hatodik óra:</b></label>
        <input name="becube_course_date_6_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_6', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_7"><b>Hetedik óra:</b></label>
        <input name="becube_course_date_7_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_7', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_8"><b>Nyolcadik óra:</b></label>
        <input name="becube_course_date_8_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_8', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_9"><b>Kilencedik óra:</b></label>
        <input name="becube_course_date_9_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_9', true); ?>">
    </p>
    <p>
        <label for="becube_course_date_10"><b>Tízedik óra:</b></label>
        <input name="becube_course_date_10_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_date_10', true); ?>">
    </p>

    <!-- Dayoffs -->
    <p>
        <label for="becube_course_dayoff_1_inputfield"><b>Első szünet:</b></label>
        <input name="becube_course_dayoff_1_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_dayoff_1', true); ?>">
    </p>
    <p>
        <label for="becube_course_dayoff_2_inputfield"><b>Második szünet:</b></label>
        <input name="becube_course_dayoff_2_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_dayoff_2', true); ?>">
    </p>
    <p>
        <label for="becube_course_dayoff_3_inputfield"><b>Harmadik szünet:</b></label>
        <input name="becube_course_dayoff_3_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_dayoff_3', true); ?>">
    </p>
    <p>
        <label for="becube_course_dayoff_4_inputfield"><b>Negyedik szünet:</b></label>
        <input name="becube_course_dayoff_4_inputfield" type="date" value="<?php echo get_post_meta($post->ID, 'becube_course_dayoff_4', true); ?>">
    </p>

    <!-- Price -->
    <p>
        <label for="becube_course_price_inputfield_label"><b>Ár:</b></label>
        <input name="becube_course_price_inputfield" type="number" value="<?php echo get_post_meta($post->ID, 'becube_course_price', true); ?>">
    </p>

    <!-- Computer provide -->
    <p>
        <label for="becube_course_providecomputer_inputfield_label"><b>Számítógép igénylés:</b> </label>

        <select name='becube_course_providecomputer_inputfield' id='becube_course_providecomputer_inputfield' value="<?php echo get_post_meta($post->ID, 'becube_course_providecomputer', true); ?>">            

            <option value="COMPUTER_NOT_PROVIDED"<?php selected(get_post_meta($post->ID, 'COMPUTER_NOT_PROVIDED', true), "UNDER_ORGANIZATION"); ?>>Hozd-a-saját-géped</option>
            <option value="COMPUTER_PROVIDED"<?php selected(get_post_meta($post->ID, 'COMPUTER_PROVIDED', true), "APPLICATION_OPEN"); ?>>Igényelhetsz számítógépet</option>

        </select>

    </p>


<?php
}


/***********************************************************************
 * Save metadata                                                       *
 ***********************************************************************/
/**
 * Writes the contents of the form to the database
 */
function becube_save_course_post_data($post_id)
{
    if (array_key_exists('becube_course_type_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_type',
            $_POST['becube_course_type_inputfield']
        );
    }

    if (array_key_exists('becube_course_status_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_status',
            $_POST['becube_course_status_inputfield']
        );
    }

    if (array_key_exists('becube_course_location_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_location',
            $_POST['becube_course_location_inputfield']
        );
    }

    if (array_key_exists('becube_course_maxheadcount_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_maxheadcount',
            $_POST['becube_course_maxheadcount_inputfield']
        );
    }

    if (array_key_exists('becube_course_teacher_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_teacher',
            $_POST['becube_course_teacher_inputfield']
        );
    }

    if (array_key_exists('becube_course_starttime_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_starttime',
            $_POST['becube_course_starttime_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_1_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_1',
            $_POST['becube_course_date_1_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_2_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_2',
            $_POST['becube_course_date_2_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_3_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_3',
            $_POST['becube_course_date_3_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_4_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_4',
            $_POST['becube_course_date_4_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_5_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_5',
            $_POST['becube_course_date_5_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_6_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_6',
            $_POST['becube_course_date_6_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_7_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_7',
            $_POST['becube_course_date_7_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_8_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_8',
            $_POST['becube_course_date_8_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_9_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_9',
            $_POST['becube_course_date_9_inputfield']
        );
    }

    if (array_key_exists('becube_course_date_10_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_date_10',
            $_POST['becube_course_date_10_inputfield']
        );
    }

    if (array_key_exists('becube_course_dayoff_1_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_dayoff_1',
            $_POST['becube_course_dayoff_1_inputfield']
        );
    }

    if (array_key_exists('becube_course_dayoff_2_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_dayoff_2',
            $_POST['becube_course_dayoff_2_inputfield']
        );
    }

    if (array_key_exists('becube_course_dayoff_3_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_dayoff_3',
            $_POST['becube_course_dayoff_3_inputfield']
        );
    }

    if (array_key_exists('becube_course_dayoff_4_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_dayoff_4',
            $_POST['becube_course_dayoff_4_inputfield']
        );
    }

    if (array_key_exists('becube_course_price_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_price',
            $_POST['becube_course_price_inputfield']
        );
    }

    if (array_key_exists('becube_course_providecomputer_inputfield', $_POST)) {
        update_post_meta(
            $post_id,
            'becube_course_providecomputer',
            $_POST['becube_course_providecomputer_inputfield']
        );
    }
}

// Hook the saving function to the save_post action
add_action('save_post', 'becube_save_course_post_data');

// Adds a hidden field to the Ultimate Member registration form to submit course code. Should be filled by JS
add_action('um_after_register_fields', 'add_a_hidden_field_to_register');
function add_a_hidden_field_to_register( $args ) {
	echo '<input type="hidden" name="course_code_registration_field" id="course_code_registration_field_id" value="" />';
}

// Responsible for creating the application when the registraion form is submitted
add_action( 'um_registration_complete', 'becube_registration_complete', 10, 2 );
function becube_registration_complete( $user_id, $args ) {
    $title = get_user_meta($user_id, 'last_name', true) . " " . get_user_meta($user_id, 'first_name', true);
    $coursecode = get_user_meta($user_id, 'course_code_registration_field', true);
    create_application($title, $coursecode, $user_id);	
}

// Add shortcode to display open courses as cards
function becube_display_open_courses() { 
    global $post;
	ob_start(); ?>
    
    <h2 class="ht-section-title upcomings">Induló Tanfolyamok</h2>
    <div class="upcomings" id="upcomings-card-div">
        <?php 
        $open_courses = get_posts(array(
            'post_type' => 'courses',
            'meta_key'   => 'becube_course_status',
            'meta_value' => 'APPLICATION_OPEN'
        ));
        
        if ( $open_courses ) {
            foreach ( $open_courses as $post ) : setup_postdata( $post );?>

            <div class="card">
            <?php the_post_thumbnail(); ?>
                   
                    <h5 class="card-title"><b><?php the_description_for_course_type(get_field('becube_course_type')); ?></b></h5>
                    <ul class="list-group list-group-flush">
                        <li>Tanfolyam kódja: ​<b><?php the_title(); ?></b></li>
                        <li>​<b><?php the_days(get_field('becube_course_date_1')); ?></b></li>
                        <li>Kezdés:​ <b><?php the_field('becube_course_date_1'); ?></b></li>
                        <li>Helyszín: <b><?php the_field('becube_course_location'); ?></b></li>
                        <li>Időpont: <b><?php the_field('becube_course_starttime'); ?></b></li>
                        <li>Ára: <b><?php the_field('becube_course_price'); ?> ft</b></li>
                        <li><?php the_description_for_providecomputer(get_field('becube_course_providecomputer')); ?></li>
                        <li><?php the_waiting_list_description( $post ); ?></li>
                    </ul>
                    <div class="ht-cta-buttons jelentkezem-btn">
                    
                    <button 
                        id="<?php the_title(); ?>"
                        data-coursetype="<?php the_description_for_course_type(get_field('becube_course_type')); ?>"
                        data-days="<?php the_days(get_field('becube_course_date_1')); ?>"
                        data-startdate="<?php the_field('becube_course_date_1'); ?>"
                        class="apply-button">
                            Jelentkezem
                    </button>
                        
                    </div>
            </div>

        <?php 
            endforeach;
        }
        ?>
    </div>

    <!-- The Modal -->
    <div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">X</span>
        <h2>Jelentkezés a <b><span id="coursecode"></span></b> tanfolyamra</h2>
        <ul>
            <li><span id="coursetype"></span></li>
            <li><span id="days"></span></li>
            <li><span id="startdate"></span> kezdéssel</li>
        </ul>
        <?php echo do_shortcode('[ultimatemember form_id="1176"]'); ?>

    </div>

    </div> 
     
	<?php
	return ob_get_clean();
}

// register shortcode
add_shortcode('becube_open_courses', 'becube_display_open_courses'); 

?>