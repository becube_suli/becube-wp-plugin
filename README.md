# BeCube WP Plugin

This plugin implements the corporate governance system for BeCube programming school.

> Disclaimer: this is work in progress. This document contains a mixture of implemented and unimplemented features. Implemented features are probably also in a not so nice form.

## Goal and purpose

Coming soon...

## Dependencies

* A student user role called `um_diak`

In the development we used [Ultimate Member](https://ultimatemember.com/) plugin to handle these. The course registration modal uses UM registration form. It adds a hidden field to UM registration form via `um_after_register_fields` action and then handles its value via the `um_registration_complete` action.

## Use cases and requirements

The system needs to integrate to BeCube's current, WordPress based webpage.

### Use cases

#### User (not paying, not registered)

* I as a visitor of the website want to click directly on the course on the webpage and apply easily to the course. Then I become a registered student.
* I as a visitor of the website want to apply to be a teacher to be seen and considered by te chool. Then I become a registered teacher.

#### Student (registered)

* I as a registered student want to check information about my payments.
* I as a student can pay for the course through the website via credit card.
* I as a student can chose between paying everything at once or in parts.
* I as a student want to be able to note in the system that I participate on a course occasion to provide correct attendance information to the school. I can only do it if I am at the right place at the right time. (This can be achieved by a random code which can be seen only by the teacher only at the time of the course, and student has to write it into the system.)
* I as a student want to be able to upload my homework to get it checked by my teacher. (Maybe unittest would be a nice to have feature.)
* I as a student want to be able to see the course material, which I payed for to go through it at home.
* I as a student want to see the time schedule and register myself to be able to individually visit other courses' lessons if I cannot go to my own.
* I as a student want to see the messages from the school to me at one place.
* I as a student want to chose to get those information via e-mail or not.
* I as an already registered student I want to use my registration to apply to new courses without new registration.

#### Teacher

* I as a teacher want to see the schedule to see when I and others have classes.
* I as a teacher want to see the uploaded homework of my own students to reply to them.
* I as a teacher want to have a simple interface where I can see everything I have to do. (Nice to have: Kanban board.)
* I as a teacher want to see the list of students on the courses.
* I as a teacher want to register attendance and homework submission if automated system needs to be corrected.
* I as a teacher want to see the list of my past and future classes along with my salary information.

#### Administrator

* I as an administrator want to create new and manage existing courses via the WordPress standard admin pages.
* I as an administrator want to create new and manage existing locations via the WordPress standard admin pages.
* I as an administrator want to see the applications, invoices and messages in one place.
* I as an administrator want to see the tasks of teachers with their statuses.
* I as an administrator want to see reports 

### Solutions

#### Registration and payment

##### Not so detailed description

When a non registered user applies to a course, 2 things happens: 1) a new user ("student" type) is registered, and 2) a new application is created in the database. The applicaiton refers to the user and the course. When an already registered student applies to a course, a new application instance is created which refers to the existing user (the student). The registered student can log in to the page and can see their own personal pages.

When a new application is created and needs to be payed, new payment records (custom posts) are created. Normally one, but the student can decide to pay it in parts. These payments can be seen by the user on their profile. The payments have a status and deadline. The student is able to pay the payments via the Barion system. The system automatically detects the payment and does the administration.

They payment has to be created in accordance with the applicable discounts. Students must be able to connect their profiles to get the friend discount.

If the student pays the payment an invoice is automatically created via Billingo REST API and is sent to the user. Also in this our system the status of the payment is changed to payed.

If the student does not pay, they get notifications via the system (and email based on their settings).

If the deadline is passed without paying, the student is set to "not payed" status and is not eligible for getting the course material, send the homework or attend the courses. The student must be able to pay the payment after the missed deadline as well and in this case they will get everything retroactively.

##### Detailed description



#### User profile page

The user has to see these as he logs in:
	
* My payments
* Check in to class
* Messages
* Upload homework
* Link to calendar

#### Teacher profile page

* My courses
* Check in code
* Messages
* My data (info about me)
* Salary information
* My calendar

#### Messaging

Standard messages should have fields filled up from variables. Therse are templates.

From templates concrete messages can be created and sent to the user. User sees them on profile and depending on settings they can get in via email too.

Preferably read/unread status can be set as well as they can be opened/closed via javascript.

## Elements

The plugin implements

* Custom post types
* Short codes
* Custom pages

The plugin integrates with

* Online creditcard payment system Barion to accept payments
* Billing system Billingo to automate billing
* Google Calendar to automatically create the schedule
* (Optional) Adobe Sign to send contracts and certification

The plugin implements (among others)

* Custom registration form and other forms
* Custom mail sending features
* User, course and location lifecycle automation management

These will be detailed in the following

### Custom post types

The plugin registers a set of custom post types with editable custom fields. There are "derived fields", which are not stored in the database, but shown on the admin page of the post. These are the following:

* **Application** - 
    * **Free fields**
        * Course code - the application is to this course. Must be a title of one of the Course posts.
        * User email - An email address filed, to which person the application belongs to.
        * Status of the application
    * **Derived fields**
        * Name of the applicant - comes from the linked user
        * Name of the teacher - comes from the user through the linked course
        * Type of the course - comes from the linked course
        * Location - comes from the course and there from the location
        * Time of the classes - comes from the linked course
        * Dates - comes from the linked course
        * Linked payments with payed / unpayed amounts (list + text)
* **Course** - 
    * **Free fields**
        * Type of the course - dropdown
        * Status - dropdown
        * Location - link to a location type post
        * Max headcount - integer
        * Teacher - dropdown with the opportunity to chose one of the teacher type users.
        * Course starttime - time
        * 10 dates for each occasion (arbitrary number in future?)
        * 4 optional date fields for dayoffs (arbitrary number in future?)
        * Providecomputer - dropdown
    * **Derived fields**
        * Current headcount / max headcount - calculated from number of applications linked to this course
        * Applicants - list of applications to this course. Waiting list and observers are visibly separated.
        * Finantial info
* **Location** - 
    * **Free fields**
    * **Derived fields**
* **Message-template** - 
    * **Free fields**
    * **Derived fields**
* **Message** - 
    * **Free fields**
    * **Derived fields**
* **Payment** - 
    * **Free fields**
    * **Derived fields**
* **Review** - 
    * **Free fields**
    * **Derived fields**

### Short codes

#### Display open courses

`[becube_display_open_courses]`

Displays all the courses in open state with buttons which open modals. The modal contains a registration form pre-filled with the course code the user clicked on.

#### Custom registration form

`[becube_registration_form]`

Custom registration form.

## Plans

### Development

The task management for this project is under the Issues -> Boards -> Development. Roles for the board:

The product backlog is made up of 4 different columns:

* Must have
* Should ave
* Could have
* Won't have (now)

These are in this priority according to the [MoSCoW method](https://en.wikipedia.org/wiki/MoSCoW_method). Backlog should be considered as one continuous backlog merged from these 4 cetagories in this order. Therefore if Must have is not empty nothing can be pulled from Should have, Could have and Won't have, etc.

The work in progress limit for in progress is 1 task (as I work alone).

Tasks should represent end-to-end functionality and be in accordance with agile principles.

If backlog and this documnetation is contradicting in terms of requirements, the backlog should be considered.

### Administrator dashboard

#### Financial summary

#### Comparisions

#### Trends

#### Calendar

### Automated processes

#### Administrator task management

Administrator can get tasks and change their statuses, comment on them etc.

#### Generate contracts

When a course is created, a PDF for the teacher can be generated. In ideal case it is signed automatically and sent to the teacher. For the administrator there is only follow up or notifications that they have to do something.

#### Pay salaries

In simpler case the system tracks which occasions have been payed and not and creates tasks for the administrator to pay it. In more advanced system (if possible) the salary can be automatically payed.

### KPI's

There can be numbers to compare teachers / courses / course types / locations, etc. These can be automatically measured by the system.

Note that it needs to be decided what the real goal is, for example high attendance is not a direct goal if the students pay the prices and keep applying to more courses. Although low attendance can make problems visible or high attendance can be a sign of satisfaction.

These should be shown on pages of teachers and locations, and should be available regarding course types and time periods (e.g. for quarters or running average) to see trends. Wherever they are shown they should be in context, e.g. in a histogram, compared to other elements or to the average/median etc.

#### Note on automatic and not automatic KPI's

A KPI can come from directly asking the customers, e.g. how satisfied you are or rate your teacher. While this gives a much more direct picture, the answering rate is usually low and distorted. Therefore it is good to measure things which come "automatically" and can still show the success of a course or teacher. Otherwise it is still a good idea to send post course questionnaires to the students.

#### Attendance percentage

There should be a way to measure drop-out rate, like if 10 people started the course how many are attending after 2, 5, 10 occasions. It could be a fitted linear or exponential function and the measurement number can be the slope, or exponent, but the simplest way is to add up the numbers of all attendances and divide it by the applicants times the occasions.

#### Homework submission rate

It is easy to attend a course without listening on it, but homework submission is commitment. The percentage of submitted homework can be a good measurement to compare courses.

#### Certification rate

While rules to get certification is arbitrary and depends on the previos 2 (homework submission and attendance) it should represent a successful course clearance and commitment and be done only by those who like the course and can use it. Certification rate (how meny percents get certifiation from a course) ignores the differences between minor/mediocre students and only shows how many of them are really committed and can be a good measurement.

#### Continuation rate

Shows how many percent of a course decided to go to the advenced course or any other course. If a student, already knowing a courese decides to pay for another course that means satisfaction. And after all this is getting money again from the same person so this should be an important KPI. A problem with it can be that it gives only a little data with high error.

#### Student questionnaires

Student questionnaires are sent out after every course where they rate the teachers and course material and course as a whole. We can use it to measure how different teachers or actions affect these.