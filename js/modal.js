jQuery (document).ready (function () {

  // Get the modal
  var modal = document.getElementById("myModal");

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  var applyButtons = document.getElementsByClassName("apply-button");

  applyButtons.forEach(applyButton => {
    var courseCode = applyButton.id;
    var courseType = applyButton.getAttribute("data-coursetype");
    var days = applyButton.getAttribute("data-days");
    var startDate = applyButton.getAttribute("data-startdate");
    applyButton.onclick = function() {onApplyButtonClick(courseCode, courseType, days, startDate);};
  });
  
  function onApplyButtonClick(courseCode, courseType, days, startDate) {
    var courseCodeField = document.getElementById("course_code_registration_field_id");
    if (courseCodeField) {
      courseCodeField.value = courseCode;
    }
    var courseCodeTag = document.getElementById("coursecode");
    if (courseCodeTag) {
      courseCodeTag.innerHTML = courseCode;
    }
    var courseTypeTag = document.getElementById("coursetype");
    if (courseTypeTag) {
      courseTypeTag.innerHTML = courseType;
    }
    var daysTag = document.getElementById("days");
    if (daysTag) {
      daysTag.innerHTML = days;
    }
    var startDateTag = document.getElementById("startdate");
    if (startDateTag) {
      startDateTag.innerHTML = startDate; 
    }
    modal.style.display = "block";
  }

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
  
});