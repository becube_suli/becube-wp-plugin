<?php
/*
Plugin Name: BeCube Plugin
Description: Corporate Governance System for BeCube programming school
Version: 0.1
Author: Adam Rudolf, BeCube
*/

/*
Enqueue scripts and css
*/
function becube_enqueue_scripts() {   
    wp_enqueue_script( 'jquery' );
    
    wp_enqueue_style( 'modal_style', plugins_url('/css/modal.css', __FILE__ ) );
   
    wp_enqueue_script( 'modal_script', plugins_url('/js/modal.js', __FILE__ ), true );
}
add_action( 'wp_enqueue_scripts', 'becube_enqueue_scripts' );

include_once(plugin_dir_path(__FILE__)."custom-post-types/application.php");
include_once(plugin_dir_path(__FILE__)."custom-post-types/course.php");
include_once(plugin_dir_path(__FILE__)."custom-post-types/review.php");
include_once(plugin_dir_path(__FILE__)."custom-post-types/message-template.php");
include_once(plugin_dir_path(__FILE__)."custom-post-types/message.php");
include_once(plugin_dir_path(__FILE__)."custom-post-types/location.php");
include_once(plugin_dir_path(__FILE__)."custom-post-types/payment.php");
/*
TODO vouchers
*/


?>